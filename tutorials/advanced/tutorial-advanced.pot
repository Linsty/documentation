msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2021-03-28 20:01+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr ""

#. (itstool) path: articleinfo/title
#: tutorial-advanced.xml:6
msgid "Advanced"
msgstr ""

#. (itstool) path: articleinfo/subtitle
#: tutorial-advanced.xml:7
msgid "Tutorial"
msgstr ""

#. (itstool) path: abstract/para
#: tutorial-advanced.xml:11
msgid ""
"This tutorial covers copy/paste, node editing, freehand and bezier drawing, "
"path manipulation, booleans, offsets, simplification, and text tool."
msgstr ""

#. (itstool) path: abstract/para
#: tutorial-advanced.xml:15
msgid ""
"Use <keycombo><keycap function=\"control\">Ctrl</keycap><keycap>arrows</"
"keycap></keycombo>, <mousebutton role=\"mouse-wheel\">mouse wheel</"
"mousebutton>, or <mousebutton role=\"middle-button-drag\">middle button "
"drag</mousebutton> to scroll the page down. For basics of object creation, "
"selection, and transformation, see the Basic tutorial in "
"<menuchoice><guimenu>Help</guimenu><guimenuitem>Tutorials</guimenuitem></"
"menuchoice>."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-advanced.xml:23
msgid "Pasting techniques"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:24
msgid ""
"After you copy some object(s) by <keycombo><keycap function=\"control"
"\">Ctrl</keycap><keycap>C</keycap></keycombo> or cut by <keycombo><keycap "
"function=\"control\">Ctrl</keycap><keycap>X</keycap></keycombo>, the regular "
"<guimenuitem>Paste</guimenuitem> command (<keycombo><keycap function="
"\"control\">Ctrl</keycap><keycap>V</keycap></keycombo>) pastes the copied "
"object(s) right under the mouse cursor or, if the cursor is outside the "
"window, to the center of the document window. However, the object(s) in the "
"clipboard still remember the original place from which they were copied, and "
"you can paste back there by <guimenuitem>Paste In Place</guimenuitem> "
"(<keycombo><keycap function=\"control\">Ctrl</keycap><keycap function=\"alt"
"\">Alt</keycap><keycap>V</keycap></keycombo>)."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:34
msgid ""
"Another command, <guimenuitem>Paste Style</guimenuitem> (<keycombo><keycap "
"function=\"shift\">Shift</keycap><keycap function=\"control\">Ctrl</"
"keycap><keycap>V</keycap></keycombo>), applies the style of the (first) "
"object on the clipboard to the current selection. The “style” thus pasted "
"includes all the fill, stroke, and font settings, but not the shape, size, "
"or parameters specific to a shape type, such as the number of tips of a star."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:41
msgid ""
"Yet another set of paste commands, <guimenuitem>Paste Size</guimenuitem>, "
"scales the selection to match the desired size attribute of the clipboard "
"object(s). There are a number of commands for pasting size and are as "
"follows: Paste Size, Paste Width, Paste Height, Paste Size Separately, Paste "
"Width Separately, and Paste Height Separately."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:47
msgid ""
"<guimenuitem>Paste Size</guimenuitem> scales the whole selection to match "
"the overall size of the clipboard object(s). <guimenuitem>Paste Width</"
"guimenuitem>/<guimenuitem>Paste Height</guimenuitem> scale the whole "
"selection horizontally/vertically so that it matches the width/height of the "
"clipboard object(s). These commands honor the scale ratio lock on the "
"Selector Tool controls bar (between W and H fields), so that when that lock "
"is pressed, the other dimension of the selected object is scaled in the same "
"proportion; otherwise the other dimension is unchanged. The commands "
"containing “Separately” work similarly to the above described commands, "
"except that they scale each selected object separately to make it match the "
"size/width/height of the clipboard object(s)."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:57
msgid ""
"Clipboard is system-wide - you can copy/paste objects between different "
"Inkscape instances as well as between Inkscape and other applications (which "
"must be able to handle SVG on the clipboard to use this)."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-advanced.xml:63
msgid "Drawing freehand and regular paths"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:64
msgid ""
"The easiest way to create an arbitrary shape is to draw it using the Pencil "
"(freehand) tool (<keycap>F6</keycap>):"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:75
msgid ""
"If you want more regular shapes, use the Pen (Bezier) tool "
"(<keycombo><keycap function=\"shift\">Shift</keycap><keycap>F6</keycap></"
"keycombo>):"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:86
msgid ""
"With the Pen tool, each <mousebutton role=\"click\">click</mousebutton> "
"creates a sharp node without any curve handles, so a series of clicks "
"produces a sequence of straight line segments. <mousebutton role=\"click"
"\">click</mousebutton> and <mousebutton role=\"mouse-drag\">drag</"
"mousebutton> creates a smooth Bezier node with two collinear opposite "
"handles. Press <keycap function=\"shift\">Shift</keycap> while dragging out "
"a handle to rotate only one handle and fix the other. As usual, <keycap "
"function=\"control\">Ctrl</keycap> limits the direction of either the "
"current line segment or the Bezier handles to 15 degree increments. Pressing "
"<keycap>Enter</keycap> finalizes the line, <keycap>Esc</keycap> cancels it. "
"To cancel only the last segment of an unfinished line, press "
"<keycap>Backspace</keycap>."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:96
msgid ""
"In both freehand and bezier tools, the currently selected path displays "
"small square <firstterm>anchors</firstterm> at both ends. These anchors "
"allow you to <emphasis>continue</emphasis> this path (by drawing from one of "
"the anchors) or <emphasis>close</emphasis> it (by drawing from one anchor to "
"the other) instead of creating a new one."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-advanced.xml:104
msgid "Editing paths"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:105
msgid ""
"Unlike shapes created by shape tools, the Pen and Pencil tools create what "
"is called <firstterm>paths</firstterm>. A path is a sequence of straight "
"line segments and/or Bezier curves which, as any other Inkscape object, may "
"have arbitrary fill and stroke properties. But unlike a shape, a path can be "
"edited by freely dragging any of its nodes (not just predefined handles) or "
"by directly dragging a segment of the path. Select this path and switch to "
"the Node tool (<keycap>F2</keycap>):"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:119
msgid ""
"You will see a number of gray square <firstterm>nodes</firstterm> on the "
"path. These nodes can be <firstterm>selected</firstterm> by <mousebutton "
"role=\"click\">click</mousebutton>, <keycombo><keycap function=\"shift"
"\">Shift</keycap><mousebutton role=\"click\">click</mousebutton></keycombo>, "
"or by <mousebutton role=\"mouse-drag\">drag</mousebutton>ging a rubberband - "
"exactly like objects are selected by the Selector tool. You can also click a "
"path segment to automatically select the adjacent nodes. Selected nodes "
"become highlighted and show their <firstterm>node handles</firstterm> - one "
"or two small circles connected to each selected node by straight lines. The "
"<keycap>!</keycap> key inverts node selection in the current subpath(s) (i."
"e. subpaths with at least one selected node); <keycombo><keycap function="
"\"alt\">Alt</keycap><keycap>!</keycap></keycombo> inverts in the entire path."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:130
msgid ""
"Paths are edited by <mousebutton role=\"mouse-drag\">drag</mousebutton>ging "
"their nodes, node handles, or directly dragging a path segment. (Try to drag "
"some nodes, handles, and path segments of the above path.) <keycap function="
"\"control\">Ctrl</keycap> works as usual to restrict movement and rotation. "
"The <keycap>arrow</keycap> keys, <keycap>Tab</keycap>, <keycap>[</keycap>, "
"<keycap>]</keycap>, <keycap>&lt;</keycap>, <keycap>&gt;</keycap> keys with "
"their modifiers all work just as they do in selector, but apply to nodes "
"instead of objects. You can add nodes anywhere on a path by either double "
"clicking or by <keycombo><keycap function=\"control\">Ctrl</keycap><keycap "
"function=\"alt\">Alt</keycap><mousebutton role=\"click\">click</"
"mousebutton></keycombo> at the desired location."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:140
msgid ""
"You can delete nodes with <keycap>Del</keycap> or <keycombo><keycap function="
"\"control\">Ctrl</keycap><keycap function=\"alt\">Alt</keycap><mousebutton "
"role=\"click\">click</mousebutton></keycombo>. When deleting nodes it will "
"try to retain the shape of the path, if you desire for the handles of the "
"adjacent nodes to be retracted (not retaining the shape) you can delete with "
"<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>Del</keycap></"
"keycombo>. Additionally, you can duplicate (<keycombo><keycap function="
"\"shift\">Shift</keycap><keycap>D</keycap></keycombo>) selected nodes. The "
"path can be broken (<keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap>B</keycap></keycombo>) at the selected nodes, or if you "
"select two endnodes on one path, you can join them (<keycombo><keycap "
"function=\"shift\">Shift</keycap><keycap>J</keycap></keycombo>)."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:151
msgid ""
"A node can be made <firstterm>cusp</firstterm> (<keycombo><keycap function="
"\"shift\">Shift</keycap><keycap>C</keycap></keycombo>), which means its two "
"handles can move independently at any angle to each other; "
"<firstterm>smooth</firstterm> (<keycombo><keycap function=\"shift\">Shift</"
"keycap><keycap>S</keycap></keycombo>), which means its handles are always on "
"the same straight line (collinear); <firstterm>symmetric</firstterm> "
"(<keycombo><keycap function=\"shift\">Shift</keycap><keycap>Y</keycap></"
"keycombo>), which is the same as smooth, but the handles also have the same "
"length; and <firstterm>auto-smooth</firstterm> (<keycombo><keycap function="
"\"shift\">Shift</keycap><keycap>A</keycap></keycombo>), a special node that "
"automatically adjusts the handles of the node and surrounding auto-smooth "
"nodes to maintain a smooth curve. When you switch the type of node, you can "
"preserve the position of one of the two handles by hovering your "
"<mousebutton>mouse</mousebutton> over it, so that only the other handle is "
"rotated/scaled to match."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:164
msgid ""
"Also, you can <firstterm>retract</firstterm> a node's handle altogether by "
"<keycombo><keycap function=\"control\">Ctrl</keycap><mousebutton role=\"click"
"\">click</mousebutton></keycombo>ing on it. If two adjacent nodes have their "
"handles retracted, the path segment between them is a straight line. To pull "
"out the retracted node, <keycombo><keycap function=\"shift\">Shift</"
"keycap><mousebutton role=\"mouse-drag\">drag</mousebutton></keycombo> away "
"from the node."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-advanced.xml:174
msgid "Subpaths and combining"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:175
msgid ""
"A path object may contain more than one <firstterm>subpath</firstterm>. A "
"subpath is a sequence of nodes connected to each other. (Therefore, if a "
"path has more than one subpath, not all of its nodes are connected.) Below "
"left, three subpaths belong to a single compound path; the same three "
"subpaths on the right are independent path objects:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:188
msgid ""
"Note that a compound path is not the same as a group. It's a single object "
"which is only selectable as a whole. If you select the left object above and "
"switch to node tool, you will see nodes displayed on all three subpaths. On "
"the right, you can only node-edit one path at a time."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:193
msgid ""
"Inkscape can <guimenuitem>Combine</guimenuitem> paths into a compound path "
"(<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>K</keycap></"
"keycombo>) and <guimenuitem>Break Apart</guimenuitem> a compound path into "
"separate paths (<keycombo><keycap function=\"shift\">Shift</keycap><keycap "
"function=\"control\">Ctrl</keycap><keycap>K</keycap></keycombo>). Try these "
"commands on the above examples. Since an object can only have one fill and "
"stroke, a new compound path gets the style of the first (lowest in z-order) "
"object being combined."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:201
msgid ""
"When you combine overlapping paths with fill, usually the fill will "
"disappear in the areas where the paths overlap:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:212
msgid ""
"This is the easiest way to create objects with holes in them. For more "
"powerful path commands, see “Boolean operations” below."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-advanced.xml:218
msgid "Converting to path"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:219
msgid ""
"Any shape or text object can be <firstterm>converted to path</firstterm> "
"(<keycombo><keycap function=\"shift\">Shift</keycap><keycap function="
"\"control\">Ctrl</keycap><keycap>C</keycap></keycombo>). This operation does "
"not change the appearance of the object but removes all capabilities "
"specific to its type (e.g. you can't round the corners of a rectangle or "
"edit the text anymore); instead, you can now edit its nodes. Here are two "
"stars - the left one is kept a shape and the right one is converted to path. "
"Switch to node tool and compare their editability when selected:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:234
msgid ""
"Moreover, you can convert to a path (“outline”) the <firstterm>stroke</"
"firstterm> of any object. Below, the first object is the original path (no "
"fill, black stroke), while the second one is the result of the "
"<guimenuitem>Stroke to Path</guimenuitem> command (black fill, no stroke):"
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-advanced.xml:248
msgid "Boolean operations"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:249
msgid ""
"The commands in the <guimenu>Path</guimenu> menu let you combine two or more "
"objects using <firstterm>boolean operations</firstterm>:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:260
msgid ""
"The keyboard shortcuts for these commands allude to the arithmetic analogs "
"of the boolean operations (union is addition, difference is subtraction, "
"etc.). The <guimenuitem>Difference</guimenuitem> and <guimenuitem>Exclusion</"
"guimenuitem> commands can only apply to two selected objects; others may "
"process any number of objects at once. The result always receives the style "
"of the bottom object."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:266
msgid ""
"The result of the <guimenuitem>Exclusion</guimenuitem> command looks similar "
"to <guimenuitem>Combine</guimenuitem> (see above), but it is different in "
"that <guimenuitem>Exclusion</guimenuitem> adds extra nodes where the "
"original paths intersect. The difference between <guimenuitem>Division</"
"guimenuitem> and <guimenuitem>Cut Path</guimenuitem> is that the former cuts "
"the entire bottom object by the path of the top object, while the latter "
"only cuts the bottom object's stroke and removes any fill (this is "
"convenient for cutting fill-less strokes into pieces)."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-advanced.xml:276
msgid "Inset and outset"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:277
msgid ""
"Inkscape can expand and contract shapes not only by scaling, but also by "
"<firstterm>offsetting</firstterm> an object's path, i.e. by displacing it "
"perpendicular to the path in each point. The corresponding commands are "
"called <guimenuitem>Inset</guimenuitem> (<keycombo><keycap function=\"control"
"\">Ctrl</keycap><keycap>(</keycap></keycombo>) and <guimenuitem>Outset</"
"guimenuitem> (<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>)</"
"keycap></keycombo>). Shown below is the original path (red) and a number of "
"paths inset or outset from that original:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:293
msgid ""
"The plain <guimenuitem>Inset</guimenuitem> and <guimenuitem>Outset</"
"guimenuitem> commands produce paths (converting the original object to path "
"if it's not a path yet). Often, more convenient is the <guimenuitem>Dynamic "
"Offset</guimenuitem> (<keycombo><keycap function=\"control\">Ctrl</"
"keycap><keycap>J</keycap></keycombo>) which creates an object with a "
"draggable handle (similar to a shape's handle) controlling the offset "
"distance. Select the object below, switch to the node tool, and drag its "
"handle to get an idea:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:308
msgid ""
"Such a <firstterm>dynamic offset object</firstterm> remembers the original "
"path, so it does not “degrade” when you change the offset distance again and "
"again. When you don't need it to be adjustable anymore, you can always "
"convert an offset object back to path."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:313
msgid ""
"Still more convenient is a <firstterm>linked offset</firstterm>, which is "
"similar to the dynamic variety but is connected to another path which "
"remains editable. You can have any number of linked offsets for one source "
"path. Below, the source path is red, one offset linked to it has black "
"stroke and no fill, the other has black fill and no stroke."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:319
msgid ""
"Select the red object and node-edit it; watch how both linked offsets "
"respond. Now select any of the offsets and drag its handle to adjust the "
"offset radius. Finally, notehow you can move or transform the offset objects "
"independently without losing their connection with the source."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-advanced.xml:332
msgid "Simplification"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:333
msgid ""
"The main use of the <guimenuitem>Simplify</guimenuitem> command "
"(<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>L</keycap></"
"keycombo>) is reducing the number of nodes on a path while <emphasis>almost</"
"emphasis> preserving its shape. This may be useful for paths created by the "
"Pencil tool, since that tool sometimes creates more nodes than necessary. "
"Below, the left shape is as created by the freehand tool, and the right one "
"is a copy that was simplified. The original path has 28 nodes, while the "
"simplified one has 17 (which means it is much easier to work with in node "
"tool) and is smoother."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:348
msgid ""
"The amount of simplification (called the <firstterm>threshold</firstterm>) "
"depends on the size of the selection. Therefore, if you select a path along "
"with some larger object, it will be simplified more aggressively than if you "
"select that path alone. Moreover, the <guimenuitem>Simplify</guimenuitem> "
"command is <firstterm>accelerated</firstterm>. This means that if you press "
"<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>L</keycap></"
"keycombo> several times in quick succession (so that the calls are within "
"0.5 sec from each other), the threshold is increased on each call. (If you "
"do another Simplify after a pause, the threshold is back to its default "
"value.) By making use of the acceleration, it is easy to apply the exact "
"amount of simplification you need for each case."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:358
msgid ""
"Besides smoothing freehand strokes, <guimenuitem>Simplify</guimenuitem> can "
"be used for various creative effects. Often, a shape which is rigid and "
"geometric benefits from some amount of simplification that creates cool life-"
"like generalizations of the original form - melting sharp corners and "
"introducing very natural distortions, sometimes stylish and sometimes plain "
"funny. Here's an example of a clipart shape that looks much nicer after "
"<guimenuitem>Simplify</guimenuitem>:"
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-advanced.xml:374
msgid "Creating text"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:375
msgid ""
"Inkscape is capable of creating long and complex texts. However, it's also "
"pretty convenient for creating small text objects such as heading, banners, "
"logos, diagram labels and captions, etc. This section is a very basic "
"introduction into Inkscape's text capabilities."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:380
msgid ""
"Creating a text object is as simple as switching to the Text tool "
"(<keycap>F8</keycap>), clicking somewhere in the document, and typing your "
"text. To change font family, style, size, and alignment, open the Text and "
"Font dialog (<keycombo><keycap function=\"shift\">Shift</keycap><keycap "
"function=\"control\">Ctrl</keycap><keycap>T</keycap></keycombo>). That "
"dialog also has a text entry tab where you can edit the selected text object "
"- in some situations, it may be more convenient than editing it right on the "
"canvas (in particular, that tab supports as-you-type spell checking)."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:389
msgid ""
"Like other tools, Text tool can select objects of its own type - text "
"objects -so you can click to select and position the cursor in any existing "
"text object (such as this paragraph)."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:393
msgid ""
"One of the most common operations in text design is adjusting spacing "
"between letters and lines. As always, Inkscape provides keyboard shortcuts "
"for this. When you are editing text, the <keycombo><keycap function=\"alt"
"\">Alt</keycap><keycap>&lt;</keycap></keycombo> and <keycombo><keycap "
"function=\"alt\">Alt</keycap><keycap>&gt;</keycap></keycombo> keys change "
"the <firstterm>letter spacing</firstterm> in the current line of a text "
"object, so that the total length of the line changes by 1 pixel at the "
"current zoom (compare to Selector tool where the same keys do pixel-sized "
"object scaling). As a rule, if the font size in a text object is larger than "
"the default, it will likely benefit from squeezing letters a bit tighter "
"than the default. Here's an example:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:410
msgid ""
"The tightened variant looks a bit better as a heading, but it's still not "
"perfect: the distances between letters are not uniform, for example the “a” "
"and “t” are too far apart while “t” and “i” are too close. The amount of "
"such bad kerns (especially visible in large font sizes) is greater in low "
"quality fonts than in high quality ones; however, in any text string and in "
"any font you will probably find pairs of letters that will benefit from "
"kerning adjustments."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:417
msgid ""
"Inkscape makes these adjustments really easy. Just move your text editing "
"cursor between the offending characters and use <keycombo><keycap function="
"\"alt\">Alt</keycap><keycap>arrows</keycap></keycombo> to move the letters "
"right of the cursor. Here is the same heading again, this time with manual "
"adjustments for visually uniform letter positioning:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:430
msgid ""
"In addition to shifting letters horizontally by <keycombo><keycap function="
"\"alt\">Alt</keycap><keycap>Left</keycap></keycombo> or <keycombo><keycap "
"function=\"alt\">Alt</keycap><keycap>Right</keycap></keycombo>, you can also "
"move them vertically by using <keycombo><keycap function=\"alt\">Alt</"
"keycap><keycap>Up</keycap></keycombo> or <keycombo><keycap function=\"alt"
"\">Alt</keycap><keycap>Down</keycap></keycombo>:"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:444
msgid ""
"Of course you could just convert your text to path (<keycombo><keycap "
"function=\"shift\">Shift</keycap><keycap function=\"control\">Ctrl</"
"keycap><keycap>C</keycap></keycombo>) and move the letters as regular path "
"objects. However, it is much more convenient to keep text as text - it "
"remains editable, you can try different fonts without removing the kerns and "
"spacing, and it takes much less space in the saved file. The only "
"disadvantage to the “text as text” approach is that you need to have the "
"original font installed on any system where you want to open that SVG "
"document."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:452
msgid ""
"Similar to letter spacing, you can also adjust <firstterm>line spacing</"
"firstterm> in multi-line text objects. Try the <keycombo><keycap function="
"\"control\">Ctrl</keycap><keycap function=\"alt\">Alt</keycap><keycap>&lt;</"
"keycap></keycombo> and <keycombo><keycap function=\"control\">Ctrl</"
"keycap><keycap function=\"alt\">Alt</keycap><keycap>&gt;</keycap></keycombo> "
"keys on any paragraph in this tutorial to space it in or out so that the "
"overall height of the text object changes by 1 pixel at the current zoom. As "
"in Selector, pressing <keycap function=\"shift\">Shift</keycap> with any "
"spacing or kerning shortcut produces 10 times greater effect than without "
"Shift."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-advanced.xml:464
msgid "XML editor"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:465
msgid ""
"The ultimate power tool of Inkscape is the XML editor (<keycombo><keycap "
"function=\"shift\">Shift</keycap><keycap function=\"control\">Ctrl</"
"keycap><keycap>X</keycap></keycombo>). It displays the entire XML tree of "
"the document, always reflecting its current state. You can edit your drawing "
"and watch the corresponding changes in the XML tree. Moreover, you can edit "
"any text, element, or attribute nodes in the XML editor and see the result "
"on your canvas. This is the best tool imaginable for learning SVG "
"interactively, and it allows you to do tricks that would be impossible with "
"regular editing tools."
msgstr ""

#. (itstool) path: sect1/title
#: tutorial-advanced.xml:475
msgid "Conclusion"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-advanced.xml:476
msgid ""
"This tutorial shows only a small part of all capabilities of Inkscape. We "
"hope you enjoyed it. Don't be afraid to experiment and share what you "
"create. Please visit <ulink url=\"http://www.inkscape.org\">www.inkscape."
"org</ulink> for more information, latest versions, and help from user and "
"developer communities."
msgstr ""

#. (itstool) path: Work/format
#: advanced-f01.svg:49 advanced-f02.svg:48 advanced-f03.svg:48
#: advanced-f04.svg:48 advanced-f05.svg:48 advanced-f06.svg:48
#: advanced-f07.svg:48 advanced-f08.svg:49 advanced-f09.svg:48
#: advanced-f10.svg:48 advanced-f11.svg:49 advanced-f12.svg:48
#: advanced-f13.svg:48 advanced-f14.svg:48 advanced-f15.svg:48
#: advanced-f16.svg:48
msgid "image/svg+xml"
msgstr ""

#. (itstool) path: text/tspan
#: advanced-f08.svg:70
#, no-wrap
msgid "Original shapes"
msgstr ""

#. (itstool) path: text/tspan
#: advanced-f08.svg:81
#, no-wrap
msgid "Union"
msgstr ""

#. (itstool) path: text/tspan
#: advanced-f08.svg:86
#, no-wrap
msgid "(Ctrl++)"
msgstr ""

#. (itstool) path: text/tspan
#: advanced-f08.svg:97
#, no-wrap
msgid "Difference (Ctrl+-)"
msgstr ""

#. (itstool) path: text/tspan
#: advanced-f08.svg:102
#, no-wrap
msgid "(bottom minus top)"
msgstr ""

#. (itstool) path: text/tspan
#: advanced-f08.svg:113
#, no-wrap
msgid "Intersection"
msgstr ""

#. (itstool) path: text/tspan
#: advanced-f08.svg:118
#, no-wrap
msgid "(Ctrl+*)"
msgstr ""

#. (itstool) path: text/tspan
#: advanced-f08.svg:129
#, no-wrap
msgid "Exclusion"
msgstr ""

#. (itstool) path: text/tspan
#: advanced-f08.svg:134
#, no-wrap
msgid "(Ctrl+^)"
msgstr ""

#. (itstool) path: text/tspan
#: advanced-f08.svg:145
#, no-wrap
msgid "Division"
msgstr ""

#. (itstool) path: text/tspan
#: advanced-f08.svg:150
#, no-wrap
msgid "(Ctrl+/)"
msgstr ""

#. (itstool) path: text/tspan
#: advanced-f08.svg:161
#, no-wrap
msgid "Cut Path"
msgstr ""

#. (itstool) path: text/tspan
#: advanced-f08.svg:166
#, no-wrap
msgid "(Ctrl+Alt+/)"
msgstr ""

#. (itstool) path: text/tspan
#: advanced-f13.svg:69 advanced-f14.svg:69
#, no-wrap
msgid "Original"
msgstr ""

#. (itstool) path: text/tspan
#: advanced-f13.svg:80
#, no-wrap
msgid "Slight simplification"
msgstr ""

#. (itstool) path: text/tspan
#: advanced-f13.svg:91
#, no-wrap
msgid "Aggressive simplification"
msgstr ""

#. (itstool) path: text/tspan
#: advanced-f14.svg:80
#, no-wrap
msgid "Letter spacing decreased"
msgstr ""

#. (itstool) path: text/tspan
#: advanced-f14.svg:87 advanced-f14.svg:95 advanced-f15.svg:78
#: advanced-f16.svg:68
#, no-wrap
msgid "Inspiration"
msgstr ""

#. (itstool) path: text/tspan
#: advanced-f15.svg:69
#, no-wrap
msgid "Letter spacing decreased, some letter pairs manually kerned"
msgstr ""
